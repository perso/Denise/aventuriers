let nixpkgs = import (builtins.fetchTarball {
  name = "nixos-unstable";
  url = "https://github.com/NixOS/nixpkgs/archive/8e2b14aceb1d40c7e8b84c03a7c78955359872bb.tar.gz";
  sha256 = "0zzjpd9smr7rxzrdf6raw9kbj42fbvafxb5bz36lcxgv290pgsm8";
}) { overlays = []; config = {}; };
in
{ pkgs ? nixpkgs }:
let
  copy = pkgs.writeScriptBin "copy" ''
    #!${pkgs.stdenv.shell}

    cp "$@"
  '';
in
pkgs.mkShell {
  buildInputs = [
    pkgs.gnumake pkgs.calibre copy pkgs.inkscape_0
    (pkgs.texlive.combine {
      inherit (pkgs.texlive) scheme-small preprint fourier
      collection-plaingeneric datetime fmtcount;
    })
  ];
}

