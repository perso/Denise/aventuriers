LISTEFICHIERS=symbole.png aventuriers.tex arc_5RCDA.tex arc_6_CI.tex arc_3RC.tex arc_libeseve.tex arc_JC.tex arc_4AD.tex arc_4.5_JCS.tex intro.tex arc_CID.tex noms.tex  arc_7_JCSR.tex arc_8.tex arc_9.tex arc_10_chateau.tex arc_11_sauvetages.tex arc_12_sauvetagesvraiment.tex arc_13_retourI.tex

COMMANDE_TZ=TZ='Europe/Paris'
#COMMANDE_TZ=TZ='Australia/Adelaide'

all: aventuriers.pdf

tout: aventuriers.pdf html kindle epub

html: html/aventuriers.html

kindle: aventuriers.mobi

epub: aventuriers.epub

image: symbole.png

aventuriers.mobi: html/aventuriers.html
	nice -19 ebook-convert html/aventuriers.html aventuriers.mobi

aventuriers.epub: html/aventuriers.html
	nice -19 ebook-convert html/aventuriers.html aventuriers.epub

aventuriers.pdf: $(LISTEFICHIERS)
	pdflatex aventuriers.tex
	$(COMMANDE_TZ) pdflatex aventuriers.tex


html/aventuriers.html: $(LISTEFICHIERS)
	mkdir -p html
	$(COMMANDE_TZ) nice -19 htlatex aventuriers.tex "xhtml,charset=utf-8" " -cmozhtf -utf8" -dhtml/ -interaction=nonstopmode
#	ln -sf aventuriers.html index.html

symbole.png: symbole.svg
	mkdir -p html
#	inkscape -w 75 symbole.svg --export-filename html/symbole.png
	inkscape -D -z --file=symbole.svg --export-png=symbole.png --export-width=75
	ebb -x symbole.png

encyclo: encyclo.pdf

encyclo.pdf: encyclo.tex noms.tex
	pdflatex encyclo.tex
	pdflatex encyclo.tex
